const express = require('express')
const app = express()

const APP_PORT = process.env.PORT || 8200

app.use(express.static('dist'))

app.get('/:orderId', function (req, res) {
  if(!req.params.orderId) {
      return res.end("")
  }
  res.sendFile(__dirname + '/views/index.html')
})

app.get('/api/orders/:orderId', (req, res) => {
    if( req.params.orderId == "1U31000101" ) {
        return res.json({
           "order":{
              "priceWithoutVat":"3550,39",
              "orderNumber":"OV1-1114/2016",
              "confirmed":"Aa",
              "priceWithVat":"4296",
              "priceWithVatInLocalCurrency":"4296",
              "id":"1U31000101",
              "createdDate":"2016-11-03",
              "currencyCode":"CZK",
              "items":[
                 {
                    "description":"(WideMemo)",
                    "vatRatePercent":"21",
                    "name":"Nosné prvky auto l-7340mm, 300-80mm, zadní L-110mm",
                    "discountPercent":"0",
                    "priceForAllItems":"2962",
                    "totalPriceWithVat":"3584,02",
                    "code":"UAUN0015",
                    "position":"1",
                    "totalPriceWithoutVat":"2962",
                    "quantity":"1",
                    "unitPrice":"2962",
                    "deliveryDate":"2016-11-19"
                 },
                 {
                    "description":"(WideMemo)",
                    "vatRatePercent":"21",
                    "name":"Zadní úhelník 110mm pro závěs",
                    "discountPercent":"0",
                    "priceForAllItems":"588,5",
                    "totalPriceWithVat":"712,09",
                    "code":"UAUN0011",
                    "position":"2",
                    "totalPriceWithoutVat":"588,5",
                    "quantity":"1",
                    "unitPrice":"588,5",
                    "deliveryDate":"2016-11-19"
                 }
              ],
              "deliveryDate":"2016-11-19"
           },
           "supplier":{
              "code":"00466",
              "name":"LASMETAL s.r.o.",
              "identificationNumber":"26957833",
              "vatId":"CZ26957833",
              "address":{
                 "city":"Vyškov",
                 "street":"Tržiště 234/73",
                 "postalCode":"682 01",
                 "country":"Česká republika",
                 "countryCode":"CZ",
                 "phone":"+420 517 330 892",
                 "email":"heger@lasmetal.cz"
              }
           },
           "client":{
              "address":{
                 "city":"Moravany",
                 "street":"Modřická 700/52",
                 "postalCode":"664 48",
                 "country":"Czech Republic",
                 "countryCode":"CZ",
                 "phone":"+420 545 235 252",
                 "email":""
              }
           }
        })
    }
    return res.end("")
})

app.post('/api/orders/:orderId/confirm', function (req, res) {
  setTimeout(() => {
      res.json({"code":"OV2-474/2016", "country":"CZ", "confirmed":"A"})
  }, 1500)
})

app.listen(APP_PORT, function () {
  console.log(`Server listening on port ${APP_PORT}!`)
})
