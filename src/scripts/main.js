/*-- ANGULAR --*/
var app = angular.module('OrdersApp', [
'ngSanitize',
'mm.foundation',
'720kb.datepicker',
'ngAnimate',
'pascalprecht.translate'
])
.config(['$translateProvider', function ($translateProvider) {
  $translateProvider.translations('en', {
    "DELIVERY_DATE": "Delivery Date",
    "COMPANY": "Company",
    "ORDERED_ITEMS": "Ordered Items",
    "ORDER_CONFIRMED_SUCCESSFULLY": "Order Confirmed Successfully",
    "CONFIRMATION": "Confirmation",
    "ORDER_TOTAL": "Order Total",
    "NAME": "Name",
    "UNIT_PRICE": "Unit Price",
    "VAT": "VAT",
    "ORDERS": "Orders",
    "PRICE_WOUT_VAT": "Price Without VAT",
    "AMOUNT": "Amount",
    "END_OF_ORDER_ID": "End of Order Id",
    "CONFIRM_ORDER": "Confirm Order",
    "CONFIRMING_ORDER": "Confirming Order...",
    "TOTAL": "Total",
    "SHOW_ITEMS": "Show Items",
    "PCS": "pcs",
    "DELIVERY_ADDRESS": "Delivery Address",
    "INVALID_ORDER_NO": "Invalid order number",
    "FETCHING_TROUBLE": "Your order could not be loaded"
  });

  $translateProvider.translations('cs', {
    "DELIVERY_DATE": "Datum dodání",
    "COMPANY": "Firma",
    "ORDERED_ITEMS": "Objednané položky",
    "ORDER_CONFIRMED_SUCCESSFULLY": "Objednávka úspěšně potvrzena",
    "CONFIRMATION": "Potvrzení",
    "ORDER_TOTAL": "Celkem za objednávku",
    "NAME": "Název",
    "UNIT_PRICE": "Cena/ks",
    "VAT": "DPH",
    "ORDERS": "Objednávky",
    "PRICE_WOUT_VAT": "Cena bez DPH",
    "AMOUNT": "Množství",
    "END_OF_ORDER_ID": "Konec objednávky",
    "CONFIRM_ORDER": "Potvrdit objednávku",
    "CONFIRMING_ORDER": "Potvrzuji objednávku...",
    "TOTAL": "Celkem",
    "SHOW_ITEMS": "Ukázat položky",
    "PCS": "ks",
    "DELIVERY_ADDRESS": "Adresa dodání",
    "INVALID_ORDER_NO": "Nevalidní číslo objednávky",
    "FETCHING_TROUBLE": "Vaši objednávku se nepodařilo načíst"
  });
  var allowedLangs = ["cs", "en"];
  var lang = "en";

    var l = window.navigator.userLanguage || window.navigator.language;    
    for(var j = 0; j < allowedLangs.length; j++){
        if(l.match(allowedLangs[j])) {
            lang = allowedLangs[j];
            break;
        }
    }

  $translateProvider.preferredLanguage(lang);
}])
.directive('rightMenu', function() {
return {
    restrict: 'A',
    link: function(scope, element) {
        element.bind("click", function() {
            var canvas = document.getElementsByClassName("js-off-canvas-exit")[0];
            if (!canvas) return;
            setTimeout(function() { canvas.classList.remove("is-visible") }, 0);
        });
    }
};
})
.controller('ordersCtrl', [
"$scope", "$http", "$timeout",
function($scope, $http, $timeout) {
    $scope.showButtons = false;
    $scope.fetching = true;
    $scope.fetchingTrouble = false;
    $scope.urlOrderId = window.location.pathname.split("/")[1]
    var apiUrl = CONF.API_URL || "http://testis.veit.cz/api/orders/"
    function errorFnc(data, status) {
            $scope.orderId =null;
            $scope.fetchingTrouble=true;
            $scope.fetching = false;
        };
    var timer = $timeout(errorFnc, 2000);

    function parseLanguage(){
        if(window.navigator.languages)
            return window.navigator.languages.join(",")        
        return  window.navigator.language  || window.navigator.userLanguage || "en";
    }

    $http.get(apiUrl + $scope.urlOrderId, {
        headers: {
            "Accept-Language": parseLanguage()
        }
    })
        .then(function(res) {
            $timeout.cancel(timer)
            var data = res.data
            $scope.fetching = false;
            if(data.length == 0){
                $scope.orderId =null;
                return;
            }
            $scope.itemsInOrder = data.order.items.map(function(item) {
                item.deliveryDate = new Date(item.deliveryDate)
                return item
            })


            $scope.company = {
                name: data.supplier.name,
                address: data.supplier.address.street + ", " + data.supplier.address.city + ", " + data.supplier.address.country,
                postal: data.supplier.address.postalCode,
                vac: data.supplier.vatId
            };

             $scope.client = {
                name: "VEIT Electronics, s.r.o.",
                street: data.client.address.street,
                city: data.client.address.city,
                country: data.client.address.country,
                postal: data.client.address.postalCode
            };


            $scope.orderId = data.order.orderNumber;
            $scope.orderTotal = data.order.priceWithVat;

            $scope.orderDate = new Date(data.order.deliveryDate);
            $scope.orderDateFormat = 'dd. MM. yyyy';

            $scope.orderPriceUnit = data.order.currencyCode;
            $scope.priceWithVat = data.order.priceWithVat;
            $scope.priceVat = (parseFloat(data.order.priceWithVat.replace(",", ".")) - parseFloat(data.order.priceWithoutVat.replace(",", "."))).toFixed(2).toString().replace(".", ",");
            $scope.priceWithoutVat = data.order.priceWithoutVat;
            $scope.orderCountUnit = 'pcs';
            $scope.orderWeightUnit = 'kg';

            $scope.order = {
                state: 'NoChanges'
            };

            $scope.isConfirming = false;
            $scope.isConfirmed = data.order.confirmed == "A";
            $scope.showButtons = true;
            $scope.confirmError = false;
            $scope.activeState = 'NoChanges';
            $scope.shotItems = false;

            $scope.confirmOrder = function ($event) {
                console.log($event)
                $event.preventDefault();
                if ($scope.isConfirming) {
                    return;
                }
                $scope.isConfirming = true;
                $http.post(apiUrl + $scope.urlOrderId + "/confirm")
                    .then(
                        function(res) {
                            $scope.isConfirming = false;
                            //if( res.data.confirmed === "A" ) {
                            if( res.status == 200 ) {
                                $scope.isConfirmed = true;
                            }
                            else {
                                $scope.confirmError = "Error"
                            }
                        },
                        function(err) {
                            $scope.isConfirming = false;
                            $scope.confirmError = "Error"
                        })
    }
},errorFnc)        
//*---end of ordersCtrl---*/
}]);
