#!/usr/bin/env bash
set -e
# uncomment for debugging
#set -x

## Simple script for deploying veit-orders-ui (confirmation.veit.cz) to the server.
## Run this script from project root foolder: `./scripts/deploy.sh`
## Make sure you're connected to VPN!
## Your public ssh key must be in /root/.ssh/authorized_keys on remote machine
## Target host can be specified as a first argument - if none is passed then default "testis.veit.cz" is used
HOSTNAME=${1:-testis.veit.cz}
DNSNAME=${2:-inet.veit.cz}
PORT=${3:-3002}

APACHE_CONFIG=/etc/httpd/conf/httpd.conf
APACHE_UI_CONFIG=/etc/httpd/conf.d/veit-orders-ui.conf
WWW_DIR_ROOT=/var/www/veit-orders-ui
WWW_DIR="${WWW_DIR_ROOT}/public_html/"

echo "--------------------------------------------------------------------------"
echo "Copying web server config to: $HOSTNAME"
echo "--------------------------------------------------------------------------"
rsync -avzP --chmod=ugo=rwX scripts/apache/veit-orders-ui.conf root@$HOSTNAME:$APACHE_UI_CONFIG

echo "--------------------------------------------------------------------------"
echo "Setup the system - firewall, apache config, directory structure"
echo "--------------------------------------------------------------------------"
ssh root@$HOSTNAME << EOF
# Create  directory structure
sudo mkdir -p $WWW_DIR

# Open port used by orders UI
firewall-cmd --zone=public --add-port=$PORT/tcp --permanent
# reload firewal rules to make sure that everything is ok
firewall-cmd --reload

# Make apache listen on given port
grep -q  "Listen $PORT" $APACHE_CONFIG || printf "\n#Veit Orders UI (confirmation.veit.cz)\nListen $PORT" >> $APACHE_CONFIG
# replace variables in apache config - notice the usage of "@" instead of "/" in sed
# this is necessary because variables values contains slashes (directory paths)
sed -i -e "s@{{PORT}}@$PORT@g;" -e "s@{{WWW_DIR}}@$WWW_DIR@g;" -e "s@{{WWW_DIR_ROOT}}@$WWW_DIR_ROOT@g;" $APACHE_UI_CONFIG
EOF

echo "--------------------------------------------------------------------------"
echo "Configuring application for given environment."
echo "--------------------------------------------------------------------------"
sed -i.bak 's@API_URL.*@API_URL: "'"http://$DNSNAME/api/orders/"'"@' src/scripts/config.js

echo "--------------------------------------------------------------------------"
echo "Building distribution"
echo "--------------------------------------------------------------------------"
npm install
npm run build

# copy to remote server - notice that file permissions will be set to the defaults on destination server (--chmod=ugo=rwX)
echo "--------------------------------------------------------------------------"
echo "Deploying to server: $HOSTNAME"
echo "--------------------------------------------------------------------------"
rsync -avzP --chmod=ugo=rwX dist/ root@$HOSTNAME:$WWW_DIR

# Delay web server config reload as much as possible to minimize downtime
echo "--------------------------------------------------------------------------"
echo "Reloading web server configuration"
echo "--------------------------------------------------------------------------"
ssh root@$HOSTNAME 'systemctl reload httpd'

echo "--------------------------------------------------------------------------"
echo "Checking the deployed application"
echo "--------------------------------------------------------------------------"
./scripts/ui-check.sh $HOSTNAME $PORT

