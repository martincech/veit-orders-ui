# A basic smoke test for checking if VEIT IS UI is up & running
# This should be replaced by more sophisticated smoke tests in the future.

# uncomment to debug bash script
#set -x

HOSTNAME=${1:-"http://testis.veit.cz"}
PORT=${2:-3002}
TARGET_HOST="$HOSTNAME:$PORT"

# Ad curl: http://superuser.com/questions/272265/getting-curl-to-output-http-status-code
RESPONSE=$(curl -s -o /dev/null -w "%{http_code}" "$TARGET_HOST")

if [ $? -ne 0 ]
then
    echo "ERROR: UI at $TARGET_HOST is not running."
    exit 1;
fi

if [ "$RESPONSE" != "200" ]
then
    echo "ERROR: UI at $TARGET_HOST is broken."
    echo "RESPONSE:"
    echo $RESPONSE
    exit 2;
fi

echo "UI at $TARGET_HOST is up & running."

