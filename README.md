# VEIT Orders Confirmation UI

This is the repository for public facing application used by VEIT's customers for order confirmation.
A customer typically gets an email with a link to this application where he check the details of given order.



## Development 

`Nodejs` v 6.x or higher required
```
# install dependencies
npm install
# build static files
npm run build
# start server, default port is set to 8200
npm start
```

Server port may be set with `APP_PORT` env variable


## Deployment
Your best bet is to use [deploy.sh](scripts/deploy.sh) script for building current version & deploying it automatically to the server.

The default server is testis.veit.cz. A custom hostname can be passed as a first parameter to `deploy.sh`.
The application will be deployed to the http://hostname:3002. 

Apache config is in `/etc/httpd/conf.d/veit-orders-ui.conf`.
