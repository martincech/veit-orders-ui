'use strict'

const gulp = require('gulp')
const sass = require('gulp-sass')
const gulpCopy = require('gulp-copy')
const concat = require('gulp-concat')
const path = require('path')
const uglify = require('gulp-uglify')

const DIST_DIR = "./dist"

gulp.task('sass', function () {
  return gulp.src('./src/sass/site.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest(`${DIST_DIR}`));
});

gulp.task('copy-statics', function () {
  return gulp.src('./src/static/**')
    .pipe(gulp.dest(`${DIST_DIR}`));
});

gulp.task('copy-views', function () {
  return gulp.src('./views/*')
    .pipe(gulp.dest(`${DIST_DIR}`));
});

gulp.task('copy', ['copy-statics', 'copy-views']);

gulp.task('scripts', function () {
  return gulp.src(['./src/scripts/config.js', './src/scripts/main.js'])
    .pipe(gulp.dest(`${DIST_DIR}/js/`));
});

gulp.task('bundle:modernizr', function () {
  return gulp.src("./src/scripts/modernizr-*")
    .pipe(concat('modernizr.js'))
    .pipe(uglify())
    .pipe(gulp.dest(`${DIST_DIR}/js/`));
});

gulp.task('bundle:libs', function () {
  return gulp.src([
      "./src/scripts/respond.js",
      "./src/scripts/angular.js",
      "./src/scripts/angular-foundation.js",
      "./src/scripts/i18n/angular-locale_cs-cz.js",
      "./src/scripts/angular-sanitize.js",
      "./src/scripts/angular-animate.js",
      "./src/scripts/moment/moment.js",
      "./src/scripts/moment-timezone/moment-timezone.js",
      "./src/scripts/angularjs-datepicker/src/js/angular-datepicker.js",
      "./src/scripts/angular-translate.js"
    ])
    .pipe(concat('libs.js'))
    .pipe(uglify())
    .pipe(gulp.dest(`${DIST_DIR}/js/`));
});

gulp.task('watch', () => {
        let tasks = [
            gulp.watch('./src/**/*', ['default']),
        ];

        const onChange = (evt) => {
            console.log('\nFile "' + path.relative(__dirname, evt.path) + '" ' + evt.type + '\n');
        };

        tasks.forEach(task => task.on('change', onChange));
});

gulp.task('default', ['sass', 'bundle:libs', 'bundle:modernizr', 'copy', 'scripts'])
